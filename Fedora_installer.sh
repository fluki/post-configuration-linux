#!/bin/bash

####LOG FILE###
#LOG_FILE=/var/log/Rescue-Post-Installation-Fedora.log
#exec &>$LOG_FILE

#########################
#	FUNCTIONS	#
#########################

function ReposRPMFusion()
{
	echo "installation of RPM FUSION"
	#free repos
	dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

	#nonfree repos
	dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

	#backup repos
	dnf install -y  rpmfusion-free-release-tainted
	dnf install -y rpmfusion-nonfree-release-tainted

	dnf update -y
        echo "do you want to do something else ?"
        Menu
}

function VideosCodecs()
{
	echo "installation of video codecs"

	dnf -y install gstreamer-ffmpeg gstreamer-plugins-bad gstreamer-plugins-bad-nonfree gstreamer-plugins-ugly gstreamer-plugin-crystalhd gstreamer1-plugins-bad-freeworld gstreamer1-plugins-bad-free gstreamer1-plugins-good gstreamer1-libav ffmpeg openh264 x264 x264-libs gstreamer1-plugin-openh264 libva-vdpau-driver gstreamer1-plugins-ugly gstreamer1-libav
  echo "do you want to do something else ?"
  Menu
}

function Hobbies()
{
	echo "installation of hobbies packages"
	dnf install -y clementine vlc texlive texstudio wordgrinder
	echo "do you want to do something else ?"
	Menu
}


function AdministratorPackages()
{
	echo "installation of prefered packages"
	dnf install --assumeyes terminator minicom wireshark vim htop nmap nmon iotop iftop bmon wavemon keepass git bashtop glances neofetch weechat BitchX adapta-gtk-theme papirus-icon-theme libreoffice-icon-theme-papirus icedtea-web gparted thunderbird deja-dup flat-remix-icon-theme tidy

	echo -e "Do you use the Workstation ? \n[Y/N]"
	read gnome
	if [ $gnome == "y" -o $gnome == "Y" ] ; then
			dnf install -y gnome-tweak-tool gnome-shell-extension-dash-to-dock gnome-shell-theme-flat-remix libreoffice-draw
			dnf remove -y rhythmbox totem
	fi

	echo -e "Do you use the Mate Spin ? \n[Y/N]"
	read mate
	if [ $mate == "y" -o $mate == "Y" ] ; then
        	echo "You said Yes!"
        	echo "installation of redshift"
        	dnf install -y redshift
        	echo "Removing pre-installed packages"
        	dnf remove -y seahorse exaile compiz*

        	#echo "Adding themes in progress..."
        	#cp -r $HOME/Themes/* /usr/share/themes/

        	#echo "Adding icons in progress..."
        	#cp -r $HOME/Icons/* /usr/share/icons/

        	echo "Post-installation finished"
	fi
        echo "do you want to do something else ?"
        Menu
}


function Vivaldi()
{
	dnf config-manager --add-repo https://repo.vivaldi.com/archive/vivaldi-fedora.repo
    dnf -y install vivaldi-stable
    echo "do you want to do something else ?"
    Menu
}

function Brave()
{
   	dnf -y install dnf-plugins-core
   	dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
   	rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
   	dnf -y install brave-browser
    echo "do you want to do something else ?"
    Menu
}

function Signal()
{
	echo -e "Do you want to install Signal Desktop from Copr Rep ? \n[Y/N]"
	read signal
	if [ $signal == "y" -o $signal == "Y" ] ; then
        	dnf copr enable luminoso/Signal-Desktop
        	dnf install signal-desktop
	fi

	echo -e "Do you want to install Signal Desktop from FlatPack Rep ? \n[Y/N]"
	read signal1
	if [ $signal1 == "y" -o $signal1 == "Y" ] ; then
        	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        	flatpak install flathub org.signal.Signal
        	flatpak run org.signal.Signal
	fi
	echo "do you want to do something else ?"
	Menu
}

function Discord()
{
	echo "Downloading Discord..."
	wget -O /opt/discord.tar.gz "https://discordapp.com/api/download?platform=linux&format=tar.gz"
	tar -C /opt/ -xvf /opt/discord.tar.gz
	ls /opt/Discord/
	#Rendre Discord executable
	chmod u+x /opt/Discord/Discord

	echo "Creating symbolic link.."
	ln -s /opt/Discord/Discord /usr/bin/discord

	echo "Modifying discord.desktop.."
	sed -i 's+/usr/share/discord/Discord+/opt/Discord/Discord+g' /opt/Discord/discord.desktop
	cp /opt/Discord/discord.desktop /usr/share/applications/
    	rm /opt/discord.tar.gz
	echo "do you want to do something else ?"
    	Menu

}

function VirtualBox()
{
	echo "Adding Virtualbox repo.."
	cd /etc/yum.repos.d/
	wget http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo


	echo "repos update.."
	dnf update

	echo "Installing dependances packages..."
	dnf install binutils gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel dkms qt5-qtx11extras libxkbcommon

	echo "Installing VirtualBox.."
	dnf install VirtualBox-6.1

	echo "rebuild kernel modules"
	/usr/lib/virtualbox/vboxdrv.sh setup

	echo "Adding the user to vboxusers group"
	usermod -a -G vboxusers $USERNAME

	echo "do you want to do something else ?"
   	 Menu
}

function All()
{
	echo "Adding RPM Fusion repos..."
	ReposRPMFusion
	echo "Installing video codecs..."
	VideosCodecs
	echo "Installing prefered packages"
	AdministratorPackages
	echo "Installing Vivaldi Browser..."
	Vivaldi
	echo "Installing Brave Browser..."
	Brave
	echo "Installing Signal App..."
	Signal
	echo "Installing Discord App.."
	Discord
	echo "Installing VirtualBox"
}

#echo "Installation of additionals backgrounds"

#cp -r $HOME/Wallpapers/* /usr/share/backgrounds/

############
#   MAIN   #
############

# Check if the script is run as root only
if [ $EUID -ne 0 ]; then
	echo "This script must be run as root"
	exit 1
fi

function Menu(){
	echo "Welcome to the Post-Installation Programm"

	echo -e "1. Install RPM FUSION Repos \n2. Install Video Codecs \n3. Install Administrator packages \n4. Install Vivaldi Browser \n5. Install Brave Browser \n6. Install Signal Desktop App \n7. Install Discord App \n8. VirtualBox \n9. All \n10. Quit"

	read CHOICE

	case $CHOICE in
        	1)
                	echo "Adding RPM Fusion repos.."
                	ReposRPMFusion
        	;;
        	2)
                	echo "Installing video codecs..."
                	VideosCodecs

        	;;
        	3)
                	echo "Installing prefered packages"
                	AdministratorPackages
        	;;
        	4)
                	echo "Installing Vivaldi Browser..."
                	Vivaldi
        	;;
        	5)
                	echo "Installing Brave Browser..."
                	Brave
			;;
        	6)
                	echo "Installing Signal App..."
                	Signal
        	;;
        	7)
                	echo "Installing Discord App.."
                	Discord
        	;;
        	8)
                    echo "Installing VirtualBox"
                    VirtualBox
        	;;
            9)
                    echo "It may take a few time, thanks for your waiting :)"
	            	All
			;;
            10)
                    echo "You choose QUIT"
				    echo "Bye bye"
                    exit
        	;;

    	     *)
                    echo "Syntax : $0 [1|2|3|4|5|6|7|8|9|10]"
	esac
}

Menu
